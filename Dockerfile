FROM registry.access.redhat.com/ubi8/ubi:8.4-211

ADD certs/* /etc/pki/ca-trust/source/anchors/
RUN update-ca-trust extract

RUN yum install -y git wget

CMD ["echo", "This is a 'Purpose-Built Container', It is not meant to be ran this way. Please review the documentation on usage."]

