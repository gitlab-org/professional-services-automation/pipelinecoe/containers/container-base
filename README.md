# Container Base


# Dockerfile 

This defines the installation of certificates and adding them to the operating system's known Certificate Authority.
It installs `git` and `wget` into the container.

# Gitlab CI

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

## Any contributions you make will be under the CCL Software License

In short, when you submit code changes, your submissions are understood to be under the same [CCL License](/LICENSE) that covers the project. Feel free to contact the maintainers if that's a concern.
